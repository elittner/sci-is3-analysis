## AIM

The current repository is an archive of the script used to generate the IS3 analysis figure in the article Belt and braces: two escape ways to maintain the cassette reservoir of large chromosomal integrons. Egill Richard, Baptiste Darracq, Eloi Littner, Gael Millot, Valentin Conte, Thomas Cokelaer, Jan Engelstädter, Eduardo P.C. Rocha, Didier Mazel, Céline Loot
bioRxiv 2023.08.31.555669; doi: https://doi.org/10.1101/2023.08.31.555669 

Made available for the reproducibility and transparency of the analysis, it was not meant to be generalizable.

## AUTHORS

Eloi Littner : eloi.littner@pasteur.fr
